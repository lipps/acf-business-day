<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('neulab_acf_field_business_day') ) :


class neulab_acf_field_business_day extends acf_field {
	
	
	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct( $settings ) {
    date_default_timezone_set('Asia/Tokyo');
		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/
				$this->name = 'business_day';
		
		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/
		$this->label = __('Business day', 'acf-business-day');
		
		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/
		$this->category = 'choice';
		
		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('business_day', 'error');
		*/
		$this->l10n = array(
			'error'	=> __('Error! Please enter a higher value', 'acf-business-day'),
		);
		
		/*
		*  settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
		*/
		$this->settings = $settings;
    
		// do not delete!
    parent::__construct();
	}
	


	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	function render_field( $field ) {
		// reset vars
		$this->_values = array();
		$this->_all_checked = true;
		// ensure array
		$field['value'] = acf_get_array($field['value']);
		// hiden input
		acf_hidden_input( array('name' => $field['name']) );
		// checkbox saves an array
		$field['name'] .= '[]';
		
		/*
		*  Review the data of $field.
		*  This will show what data is available
		*/
		//echo '<pre>';
			//print_r( $field['value'] );
		//echo '</pre>';
		/*
		* 定休日(曜日)を取得する
		* 曜日は日曜-土曜(0-6)が文字列で詰められた配列で、ACFのcheckbox形式の
		* fixed_holidaysという名前で実装されていることをexplicitに想定している
		* fixed_holidaysが存在しない場合は、定休日はないものと判断する
		*/
		$fixed_holidays = get_post_meta(get_the_ID(), 'fixed_holidays', true);
    if( empty($fixed_holidays) ) {
      $fixed_holidays = Array();
    }
		?>

    <!-- 今月分だけでなく翌月分の表示も行いたいので2つテーブルを並べる -->
    <table><tr>
    <td>
<!-- 今月のカレンダーの表示 -->
<?php echo $this->holiday_calendar($this->this_year(), $this->this_month(), $field, $fixed_holidays); ?>
    </td>
    <td>
<!-- 翌月のカレンダーの表示 -->
<?php echo $this->holiday_calendar($this->year_of_the_next_month(), $this->next_month(), $field, $fixed_holidays); ?>
    </td>
    <td>
<!-- 翌々月のカレンダーの表示 -->
<?php echo $this->holiday_calendar($this->year_of_the_nn_month(), $this->nn_month(), $field, $fixed_holidays); ?>
    </td>
    </tr></table>

		<?php
	}


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/
	function input_admin_enqueue_scripts() {
		// vars
		$url = $this->settings['url'];
		$version = $this->settings['version'];
		
		// register & include CSS
		wp_register_style('acf-business-day', "{$url}assets/css/style.css", array('acf-input'), $version);
		wp_enqueue_style('acf-business-day');
	}

	
	/*
	* カレンダーを書くためのユーティリティ関数群
	*/
  function this_year() {
	  $year = (int)date("Y");
	  return $year;
	}

	function this_month() {
	  $month = (int)date("m");
	  return $month;
	}

	function next_month() {
	  $month = (int)date("m");
	  $month = $month + 1 > 12 ? 1 : $month + 1;
	  return $month;
	}

	function nn_month() {
	  $month = (int)date("m");
	  $month = $month + 2 > 12 ? $month - 10 : $month + 2;
	  return $month;
	}

	function year_of_the_next_month() {
	  $year =(int)date("Y");
	  $month = (int)date("m");
	  $year = $month === 12 ? $year + 1 : $year;
	  return $year;
	}

	function year_of_the_nn_month() {
	  $year =(int)date("Y");
	  $month = (int)date("m");
	  $year = $month > 10 ? $year + 1 : $year;
	  return $year;
	}

  function first_weekday_of_the_month($year, $month) {
	  $weekday = (int)date('w', mktime(0, 0, 0, $month, 1, $year));
    return $weekday;
  }
 
	function last_day_of_the_month($year, $month) {
	  $last_day = (int)date('j', mktime(0, 0, 0, $month + 1, 0, $year));
	  return $last_day;
	}

	function date_string($year, $month, $day) {
	  return date("Y-m-d", mktime(0, 0, 0, $month, $day, $year));
	}

	function calendar_header() {
	  $header ='<thead><tr>'.
               '<th>日</th> <th>月</th> <th>火</th> <th>水</th> <th>木</th> <th>金</th> <th>土</th>'.
             '</tr></thead>';
    return $header;
	}

  function checkbox($field, $today, $iWeekDay, $fixed_holidays) {
    $box = "<input type=\"checkbox\" ".
		       "name=\"".esc_attr($field['name'])."\" ". 
		       "value=\"".esc_attr($today)."\" ";
		if ( in_array( (string)$iWeekDay, $fixed_holidays, true) ) {
		  $box .= "checked=\"checked\" ".
		          "class=\"checked-in-advance\" ";
    } elseif ( in_array($today, $field['value'], true) ) {
    	$box .= "checked=\"checked\" ".
		          "class=\"checked-in-advance\" ";
		}
		$box .= "/>";
		return $box;
  }
  
	/*
	* カレンダーの表示関数
	*/
	function holiday_calendar($year, $month, $field, $fixed_holidays) {
    $cal = '';
    $cal .= "<table class=\"monthly-calendar\">\r\n";
    $cal .= "<caption>".$year."年".$month."月"."</caption>\r\n";
    $cal .= $this->calendar_header()."\r\n";
    $cal .= "<tbody>\r\n";
    $iDay = 1;
    $OutOfMonth = false;
    while (!$OutOfMonth) {
      $cal .= "  <tr>\r\n";
        for ($iWeekDay=0; $iWeekDay<7; $iWeekDay++) { 
          $cal .= "    <td>";
          if ( $iDay===1 && $iWeekDay < $this->first_weekday_of_the_month($year,$month) ) {
            // 1日が始まる曜日まではスキップ
		        $cal .= "</td>\r\n";
    		    continue;
	  	    } elseif( $OutOfMonth ) {
	    	    // 月の最終日を以降はスキップ
            $cal .= "</td>\r\n";
          } else {
            $cal .= $iDay;
            $today = $this->date_string($year,$month, $iDay);
            $cal .= "<br/>";
            $cal .= $this->checkbox($field, $today, $iWeekDay, $fixed_holidays);
            $cal .= "</td>\r\n"; 
          }
          $iDay++;
		      if (!( $iDay < $this->last_day_of_the_month($year, $month) + 1)) {
 		        $OutOfMonth = true;
          }
        }
      if (!$OutOfMonth) {
        $cal .=  "  </tr>\r\n";
      }
    }
    $cal .= "</tbody>";
    $cal .= "</table>\r\n";
    return $cal;
  }
}




// initialize
new neulab_acf_field_business_day( $this->settings );


// class_exists check
endif;

?>